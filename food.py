import random
import pygame

class food:

	RADIUS = 10

	def __init__(self, width, height, border, snake):

		onSnake = True
		while onSnake:
			self.x = random.randint(border//20, (width - border)//20 - 1) * 20 + 10
			self.y = random.randint(border//20, (height - border)//20 - 1)* 20 + 10
			for i in range(len(snake.body)):
				if snake.body[i].x == self.x and snake.body[i].y == self.y:
					break
				if i == len(snake.body) - 1:
					onSnake = False

	def show(self, screen, colour):
		pygame.draw.circle(screen, colour, (self.x, self.y), self.RADIUS)

	def isOn(self, head):
		if head.x == self.x and head.y == self.y:
			return True
		return False