import pygame

class outro:
	def __init__(self, screen, width, height, level):
		
		myFont = pygame.font.SysFont(None, 130) 
		scoreFont = pygame.font.SysFont(None, 80)

		textSurface = myFont.render('YOU DIED', False, (0, 255, 0))
		scoreSurface = scoreFont.render('Score = ' + str(level), False, (0, 255, 0))
		
		textRect = textSurface.get_rect()
		scoreRect = scoreSurface.get_rect()

		textRect.center = (width//2, height//2)
		scoreRect.center = (width//2, height//2 + 70)

		screen.blit(textSurface, textRect)
		screen.blit(scoreSurface, scoreRect)
		pygame.display.flip()

		while True:
			e = pygame.event.poll()
			while e.type != pygame.KEYDOWN and e.type != pygame.NOEVENT:
				e = pygame.event.poll()

			if e.type == pygame.KEYDOWN and e.key == pygame.K_RETURN:
				screen.fill(pygame.Color('black'))
				break
			elif e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE:
				pygame.quit()
				quit()