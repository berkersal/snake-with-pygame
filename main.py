from menu import menu
from snake import snake
from food import food
from outro import outro
import pygame

def drawScene():
	pygame.draw.rect(screen, snakeColour, pygame.Rect((0,0), (width, border)))
	pygame.draw.rect(screen, snakeColour, pygame.Rect((0, border), (border, height - border)))
	pygame.draw.rect(screen, snakeColour, pygame.Rect((border, height - border), (width - border, border)))
	pygame.draw.rect(screen, snakeColour, pygame.Rect((width - border, border), (border, height - 2 * border)))

width = 1920
height = 1080
border = 20
gridSize = 20
speed = 0
score = 0

snakeColour = pygame.Color('white')
backColour = pygame.Color('black')
headColour = pygame.Color('green')
foodColour = pygame.Color('red')
clock = pygame.time.Clock()

screen = pygame.display.set_mode((width, height), pygame.FULLSCREEN)

pygame.mouse.set_visible(False)
menu(screen, width, height)
drawScene()


snakeInGame = snake(110, 90, 'r', gridSize)
foodInGame = food(width, height, border, snakeInGame)
foodInGame.show(screen, foodColour)

while True:
	
	clock.tick(15 + speed)
	for i in range(len(snakeInGame.body)):
		snakeInGame.body[i].show(screen, backColour)

	e = pygame.event.poll()
	while e.type != pygame.KEYDOWN and e.type != pygame.NOEVENT:
		e = pygame.event.poll()
	
	if e.type == pygame.KEYDOWN:
		if e.key == pygame.K_ESCAPE:
			break
		elif e.key == pygame.K_UP and snakeInGame.direction != 'd':
			snakeInGame.direction = 'u'
		elif e.key == pygame.K_DOWN and snakeInGame.direction != 'u':
			snakeInGame.direction = 'd'
		elif e.key == pygame.K_LEFT and snakeInGame.direction != 'r':
			snakeInGame.direction = 'l'
		elif e.key == pygame.K_RIGHT and snakeInGame.direction != 'l':
			snakeInGame.direction = 'r'
	
	
	status = snakeInGame.go(width, height, border, foodInGame)

	if status == -1:
		screen.fill(pygame.Color('black'))
		outro(screen, width, height, score)
		drawScene()
		snakeInGame = snake(110, 90, 'r', gridSize)
		foodInGame = food(width, height, border, snakeInGame)
		foodInGame.show(screen, foodColour)
		status = 0
		speed = 0
		score = 0
		continue
		
	if foodInGame.isOn(snakeInGame.body[0]):
		if speed < 15:
			speed += 1
		score += 1
		foodInGame = food(width, height, border, snakeInGame)

	snakeInGame.body[0].show(screen, headColour)
	foodInGame.show(screen, foodColour)
	for i in range(1, len(snakeInGame.body)):
		snakeInGame.body[i].show(screen, snakeColour)


	pygame.display.flip()



pygame.quit()

