from bodyPart import bodyPart
class snake:
	startLength = 5

	def __init__(self, headX, headY, direction, gridSize):
		self.headX = headX
		self.headY = headY
		self.direction = direction
		self.gridSize = gridSize
		self.body = []
		for i in range(self.startLength):
			self.body.append(bodyPart(headX - i * self.gridSize, headY))


	def go(self, width, height, border, food):
		self.body.insert(0, bodyPart(self.body[0].nextCorX(self.direction, self.gridSize), self.body[0].nextCorY(self.direction, self.gridSize)))
		if not food.isOn(self.body[0]):
			self.body.pop()
		if self.body[0].x <= border or self.body[0].x >= width - border or self.body[0].y <= border or self.body[0].y >= height - border:
			return -1
		for i in range(4, len(self.body)):
			if self.body[0].x == self.body[i].x and self.body[0].y == self.body[i].y:
				return -1
		return 0