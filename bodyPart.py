import pygame

class bodyPart:

	RADIUS = 10

	def __init__(self, x, y):
		self.x = x
		self.y = y

	def nextCorX(self, direction, gridSize):
		if direction == 'r':
			return self.x + gridSize
		elif direction == 'l':
			return self.x - gridSize
		else:
			return self.x

	def nextCorY(self, direction, gridSize):
		if direction == 'u':
			return self.y - gridSize
		elif direction == 'd':
			return self.y + gridSize
		else:
			return self.y


	def show(self, screen, colour):
		pygame.draw.circle(screen, colour, (self.x, self.y), self.RADIUS)