import pygame

class menu:
	def __init__(self, screen, width, height):

		pygame.font.init()

		myfont = pygame.font.SysFont(None, 130) 
		textsurface = myfont.render('SNAKE', False, (0, 255, 0))
		textRect = textsurface.get_rect()
		textRect.center = (width//2, height//2)
		screen.blit(textsurface, textRect)
		pygame.display.flip()

		while True:
			e = pygame.event.poll()
			while e.type != pygame.KEYDOWN and e.type != pygame.NOEVENT:
				e = pygame.event.poll()

			if e.type == pygame.KEYDOWN and e.key == pygame.K_RETURN:
				screen.fill(pygame.Color('black'))
				break
			elif e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE:
				pygame.quit()

